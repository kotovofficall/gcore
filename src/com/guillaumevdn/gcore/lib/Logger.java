package com.guillaumevdn.gcore.lib;

import org.bukkit.Bukkit;

import com.guillaumevdn.gcore.GCore;

public class Logger {

	// ------------------------------------------------------------
	// Logger level
	// ------------------------------------------------------------

	public enum Level {
		INFO("§f", "§f§l", "INFO: "),
		SUCCESS("§d", "§d§l", "SUCCESS: "),
		WARNING("§e", "§6", "WARNING: "),
		DEBUG("§5", "§d", "DEBUG: "),
		SEVERE("§4", "§c", "ERROR: ");

		private String color, varColor, prefix;

		private Level(String color, String varColor, String prefix) {
			this.color = color;
			this.varColor = varColor;
			this.prefix = prefix;
		}

		public String getColor() {
			return color;
		}

		public String getVarColor() {
			return varColor;
		}

		public String getPrefix() {
			return prefix;
		}
	}

	// ------------------------------------------------------------
	// Log
	// ------------------------------------------------------------

	public static void log(Level level, GPlugin plugin, String message) {
		log(level, plugin, message, true);
	}

	public static void log(Level level, GPlugin plugin, String message, boolean color) {
		log(level, plugin.getName(), message, color);
	}

	public static void log(Level level, String prefix, String message) {
		log(level, prefix, message, true);
	}

	public static void log(Level level, String prefix, String message, boolean color) {
		if (level.equals(Level.DEBUG) && !(GCore.inst().getConfiguration() == null || GCore.inst().getConfiguration().getBoolean("show_debug", true))) {
			return;
		}
		Bukkit.getConsoleSender().sendMessage((color ? level.getColor() : "") + "[" + prefix + "] " + level.getPrefix() + message.replace("<", level.getVarColor()).replace(">", level.getColor()));
	}

}
