package com.guillaumevdn.gcore.lib.util.input;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public interface LocationInput {

	// methods
    void onChoose(Player player, Location value);

}
