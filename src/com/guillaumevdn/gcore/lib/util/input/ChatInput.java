package com.guillaumevdn.gcore.lib.util.input;

import org.bukkit.entity.Player;

public interface ChatInput {

	// methods
    void onChat(Player player, String value);

}
