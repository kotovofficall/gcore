package com.guillaumevdn.gcore.lib.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.guillaumevdn.gcore.data.GUser;

/**
 * Triggered when a GUser finished loading
 */
public class GUserPulledEvent extends Event {

	// base
	private GUser user;
	private Context context;

	public GUserPulledEvent(boolean async, GUser user, Context context) {
		super(async);
		this.user = user;
		this.context = context;
	}

	// get
	public GUser getUser() {
		return user;
	}

	public Context getContext() {
		return context;
	}

	// handlers
	private static final HandlerList handlers = new HandlerList();

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	// context
	public static enum Context {

		JOIN,
		PROFILE_CHANGE,
		USER_OPERATOR

	}

}
