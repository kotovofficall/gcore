package com.guillaumevdn.gcore.lib.gui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.plugin.Plugin;

import com.guillaumevdn.gcore.lib.util.Utils;
import com.guillaumevdn.gcore.lib.versioncompat.sound.Sound;

public abstract class ShowcaseRowsGUI extends FilledGUI {

	// base
	private List<Row> rows = new ArrayList<Row>();
	private Sound clickSound;

	public ShowcaseRowsGUI(Plugin plugin, String name, int size, List<Integer> regularItemSlots, boolean unregisterOnClose, Sound clickSound) {
		super(plugin, name, size, Utils.asList(regularItemSlots));
		this.clickSound = clickSound;
	}

	// get
	public List<Row> getRows() {
		return rows;
	}

	// methods
	public void addRow(Row row) {
		if (!rows.contains(row)) {
			// remove showcase slots from regular item slots
			getRegularItemSlots().removeAll(row.getSlots());
			rows.add(row);
		}
	}

	// row
	public class Row {

		// base
		private List<Integer> slots;
		private List<Integer> currentSlots;
		private List<ClickeableItem> items;
		private int startItemIndex = 0;

		public Row(List<Integer> slots) {
			this(slots, null);
		}

		public Row(List<Integer> slots, List<ClickeableItem> items) {
			this.slots = slots;
			this.currentSlots = Utils.asList(slots);
			this.items = items != null ? items : new ArrayList<ClickeableItem>();
		}

		// get
		public List<Integer> getSlots() {
			return slots;
		}

		public List<ClickeableItem> getItems() {
			return Collections.unmodifiableList(items);
		}

		// methods
		public void addItem(ClickeableItem item, boolean update) {
			items.add(item);
			if (update) update();
		}

		public void removeItem(String itemId, boolean update) {
			for (ClickeableItem item : items) {
				if (item.getItemData().getId().equals(itemId)) {
					items.remove(item);
					if (update) update();
					return;
				}
			}
		}

		public void replaceItems(List<ClickeableItem> items, boolean update) {
			startItemIndex = 0;
			this.items.clear();
			this.items.addAll(items);
			if (update) update();
		}

		public void clear(boolean update) {
			startItemIndex = 0;
			this.items.clear();
			if (update) update();
		}

		public void update() {
			// clear slots
			for (int slot : slots) {
				removePersistentItem(slot);
			}
			// update start index
			if (startItemIndex < 0) startItemIndex = 0;
			else if (startItemIndex >= items.size()) startItemIndex = items.size() - 1;
			// must add page controls
			currentSlots = Utils.asList(slots);
			if (items.size() > slots.size()) {
				// previous page item
				if (startItemIndex != 0) {
					setPersistentItem(new ClickeableItem(GUI.PREVIOUS_PAGE_ITEM.cloneWithIdAndSlot("back_row_" + UUID.randomUUID(), currentSlots.remove(0))) {
						@Override
						public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
							startItemIndex -= slots.size();
							if (startItemIndex < 0) startItemIndex = 0;
							update();
							// sound
							if (clickSound != null) {
								clickSound.play(player);
							}
						}
					});
				}
				// next page item
				if (items.size() - startItemIndex + 1 > slots.size()) {
					setPersistentItem(new ClickeableItem(GUI.NEXT_PAGE_ITEM.cloneWithIdAndSlot("back_row_" + UUID.randomUUID(), currentSlots.remove(currentSlots.size() - 1))) {
						@Override
						public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
							startItemIndex += slots.size() - 1;
							update();
							// sound
							if (clickSound != null) {
								clickSound.play(player);
							}
						}
					});
				}
			}
			// add content
			if (!items.isEmpty()) {
				int index = startItemIndex - 1;
				for (int slot : currentSlots) {
					if (++index >= items.size()) break;
					ClickeableItem item = items.get(index);
					item.getItemData().setSlot(slot);
					setPersistentItem(item);
				}
			}
			// update first slot ffs
			updateFirstSlot();
		}

	}

}
