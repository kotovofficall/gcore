package com.guillaumevdn.gcore.lib.gui;

public enum InventoryClickType {

	TOP,
	BOTTOM,
	OUTSIDE

}
