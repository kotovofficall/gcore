package com.guillaumevdn.gcore.lib.parseable;

public interface ParseableContainment<T> {

	void replaceContaining(T element);

}
