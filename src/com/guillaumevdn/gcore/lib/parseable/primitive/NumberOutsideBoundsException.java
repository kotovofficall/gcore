package com.guillaumevdn.gcore.lib.parseable.primitive;

public class NumberOutsideBoundsException extends Throwable {

	private static final long serialVersionUID = -275930528298656243L;

	public NumberOutsideBoundsException() {
		super();
	}

	public NumberOutsideBoundsException(String message) {
		super(message);
	}

}
