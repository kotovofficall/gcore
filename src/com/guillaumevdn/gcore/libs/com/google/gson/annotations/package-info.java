/**
 * This package provides annotations that can be used with {@link com.google.gson.Gson}.
 * 
 * @author Inderjeet Singh, Joel Leitch
 */
package com.guillaumevdn.gcore.libs.com.google.gson.annotations;