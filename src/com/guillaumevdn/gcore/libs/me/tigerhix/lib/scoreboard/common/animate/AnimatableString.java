package com.guillaumevdn.gcore.libs.me.tigerhix.lib.scoreboard.common.animate;

public interface AnimatableString
{
    String current();
    String next();
    String previous();
}
